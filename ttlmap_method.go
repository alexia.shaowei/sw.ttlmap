package swttlmap

import (
	"time"
)

func (ref *TTLMap) Len() int {
	ref.mux.RLock()
	defer ref.mux.RUnlock()

	return len(ref.mp)
}

func (ref *TTLMap) Get(key string) (interface{}, bool) {
	ref.mux.Lock()
	defer ref.mux.RUnlock()

	elem, ok := ref.mp[key]
	if !ok || time.Now().UnixNano() > elem.exp {
		return nil, false
	}

	return elem.val, true
}

func (ref *TTLMap) Put(key string, val interface{}, exp int) {
	ref.mux.Lock()
	ref.mp[key] = elem{
		val: val,
		exp: time.Now().Add(time.Duration(exp)).UnixNano(),
	}

	ref.mux.Unlock()
}

func (ref *TTLMap) clear() {

	tk := time.NewTicker(cst_CLEAR_TICK)

	for {

		<-tk.C

		select {
		case <-ref.done:
			return

		default:
			ref.mux.Lock()

			for k, v := range ref.mp {
				if time.Now().UnixNano() >= v.exp {
					delete(ref.mp, k)
				}
			}

			ref.mux.Unlock()
		}

	}
}
