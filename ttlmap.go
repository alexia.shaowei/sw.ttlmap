package swttlmap

import (
	"sync"
	"time"
)

type TTLMapINTF interface {
	Get(key string) (interface{}, bool)
	Put(key string, val interface{}, exp int)
	Len() int

	clear(exp time.Duration)
}

type elem struct {
	val interface{}
	exp int64
}

type TTLMap struct {
	mp  map[string]elem
	mux *sync.RWMutex

	done chan struct{}
}
