package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("zygote::main()")

	// tk := time.NewTicker(time.Second)

	// for {
	// 	<-tk.C
	// 	fmt.Println("XX")

	// }

	for range time.Tick(time.Second) {

		fmt.Println("X")

	}

}
