package swttlmap

import (
	"runtime"
	"sync"
	"time"
)

func New(clrExp time.Duration) *TTLMap {
	ttlmap := &TTLMap{
		mp:  make(map[string]elem),
		mux: new(sync.RWMutex),

		done: make(chan struct{}),
	}

	go ttlmap.clear()

	runtime.SetFinalizer(ttlmap, func(_ interface{}) {
		ttlmap.done <- struct{}{}
	})

	return ttlmap
}
